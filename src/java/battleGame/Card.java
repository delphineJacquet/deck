package battleGame;

public class Card implements Comparable<Card> {

	private final CardLevel cardLevel;
	private final CardColor cardColor;

	private Card(CardLevel cardLevel, CardColor cardColor) {
		this.cardLevel = cardLevel;
		this.cardColor = cardColor;
	}

	public static Card createCard(CardLevel cardLevel, CardColor cardColor) {
		return new Card(cardLevel, cardColor);
	}

	public CardLevel getCardLevel() {
		return this.cardLevel;
	}

	public CardColor getCardColor() {
		return this.cardColor;
	}

	@Override
	public int compareTo(Card cardToCompare) {
		return Integer.compare(this.getCardLevel().getValue(), cardToCompare
				.getCardLevel().getValue());
	}

}
