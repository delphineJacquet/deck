package battleGame;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class CardDeck {
	
	private final List<Card> initialSetOfCards;
	private final List<Card> playingSetOfCards;
	
	public CardDeck(){
		initialSetOfCards = new ArrayList<Card>();
		for(CardColor cardColor : CardColor.values()){
			initialSetOfCards.addAll(createCardsForColor(cardColor));
		}
		playingSetOfCards = initialSetOfCards;
	}

	private Set<Card> createCardsForColor(CardColor cardColor) {
		Set<Card> cardsForColor = new HashSet<>();
		for(CardLevel cardLevel : CardLevel.values()){
			cardsForColor.add(Card.createCard(cardLevel,cardColor));
		}
		return cardsForColor;
	}
	
	public Card draw() throws EmptyDeckException{
		if(playingSetOfCards.isEmpty()){
			throw new EmptyDeckException();
		}
		int numberOfCardLeft = playingSetOfCards.size();
		Random generator = new Random();
		int randomIndex = generator.nextInt(numberOfCardLeft);
		Card myCard = playingSetOfCards.get(randomIndex);
		playingSetOfCards.remove(randomIndex);
		return myCard;
	}
	
	public void reShuffle(){
		playingSetOfCards.clear();
		playingSetOfCards.addAll(initialSetOfCards);
	}
	
	public int getNumberOfCardLeft(){
		return playingSetOfCards.size();
	}
	
	

}
