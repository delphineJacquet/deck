package battleGame;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class CardDeskTest {

	private CardDeck cardDeck52;



	@Before
	public void setUp(){
		cardDeck52 = new CardDeck();
	}
	
	@Test(expected= EmptyDeckException.class)
	public void testDrawingMoreThan52CardRaiseException() throws EmptyDeckException {
		for(int i = 0; i<53;i++){
			cardDeck52.draw();
		}
	}
	
	@Test
	public void shouldDrawACard() throws EmptyDeckException{
		Card card = cardDeck52.draw();
		assertNotNull(card);
	}
	
	
	
	@Test
	public void sizeOfTheDeskIsCorrect(){
		CardDeck cardDesk52 = new CardDeck();
		assertEquals(52, cardDesk52.getNumberOfCardLeft(),0.00001);
	}

}
